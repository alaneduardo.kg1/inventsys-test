# Company no.1 Interview Test

## Getting started

Please find here all documentation related with release, code architecture, workflow and requirements strategies for the proposed test. It is possible to find all the diagrams presented here in this [Mural board](https://app.mural.co/t/alansroom0906/m/alansroom0906/1694939997332/268f7d6df4a2d13fb5954255e79b4349e188bcc8?sender=u08372bbcb5024974613b6901)

Prerequisites:
- Docker
- Docker Compose

### Installing

Run `$ docker-compose up -d` after installing, you can check the installation on http://localhost and http://localhost/phpinfo.php

### Unit tests

Run `$ docker exec -it inventsys-test-web composer unitTest`

### Acceptance tests

Run `$ docker exec -it inventsys-test-web composer acceptanceTest`

## CI / CD strategy

![CI/CD Strategy](/uploads/caf6f3a3872ac31706cf91e7071baa2b/image.png)

### Build Stage
- **Check style:** it is performed a check style and standards in the code to see if it follows the PSR recommendations and spacements.
- **Build docker image:** After passing the code standards check, a new docker image with the latest code is generated.

### Test Stage
- **Unit test:** A unit test is a type of software test that focuses on components of a software product. The purpose is to ensure that each unit of software code works as expected. A unit can be a function, method, module, object, or other entity in an application’s source code.
- **Acceptance test:** Acceptance testing is the practice of running high-level, end-to-end tests to ensure that a system follows spec. Acceptance tests are derived from acceptance criteria, which define how an application responds to user actions or events.
Acceptance tests shift attention towards the end goal: shipping software that fulfills a business need. They cross the gap between developers and end-users, ensuring that the application works in the real world.

> All the unit and acceptance tests will run over the generated docker image in the previous step

### Deploy Stage
- **Deploy:** Apply the deployment routines to the infrastructure orchestrator (Kubernetes, Rancher, Docker Swarm, and etc.) and update the report pages with the latest tests reports and build info.

## Code Architecture strategy
![image](/uploads/3a5bfa28052e3090c7f7c5af8413b36f/image.png)

- `Application`: Low-policy layer where we have our working application, with libraries, frameworks, and etc.
    - `Entrypoint`: Entrypoint (or driving) actors are the ones that initiate the interaction, and are always depicted on the left side. For example, a driving adapter could be a controller which is the one that takes the (user) input and passes it to the Application via a Port.
    - `Infrastructure`: Infrastructure (or driven) actors are the ones that are “kicked into behavior” by the Application. For example, a database Adapter is called by the Application so that it fetches a certain data set from persistence.
- `Business Rules`: High-policy layer where all business domain code lives. The core is divided into:
    - `Rule`: the system business rules
    - `Query`: the requests and data calculations will be found here


- `Bounded Context:` Domain Driven Design (DDD) deals with large models by dividing it into different **Bounded Context** and explicit their relationships. The bounded Context can be defined as a logical boundary of a domain where particular terms and rules apply consistently. Inside this boundary, all terms, definitions, and concepts form the Ubiquitous Language.

- `Shared Kernel:` A pattern that describes a relationship between two bounded contexts. This pattern reduces the code duplication and prevents unwanted direct access between classes.

### Solution Diagram

Please find below the diagram about how the use cases interact with each other.

![image](/uploads/4f229ccbfb72f341e823adeceb3f8154/image.png)

### Code Architecture Anatomy

![image](/uploads/254a498a922223edaddaa8a3f7a2b043/image.png)

## Workflow Strategy

![image](/uploads/6b1bbf3c94d5fd4dfe8d723dc61d96ee/image.png)

## Workshop Strategy

![image](/uploads/2ae991cce0d37a3578d2235823926103/image.png)

For the workshops, I am using a technique called Example Mapping, from Behavior Driven Development guidelines (see the book "The BDD Books: Discovery"). By definition, example mapping is a technique for fleshing out and gaining clarity around the acceptance criteria for a given story. It is based on the idea that multiple examples of specific cases convey information better than a single bad abstraction of a concept. This technique uses a pack of 4-coloured index cards and some pens to capture these different types of information as the conversation unfolds. As we talk, we capture them on index cards, and arrange them in a map:

![](https://cucumber.io/cucumber/media/blog/ghost/map.png)

The orange card is a poethic license to have, in the same view, which epic all the user stories belong to.
