<?php

declare(strict_types=1);

namespace App\ObjectManagement\Application\ImportObjectDataFromSpreadsheet\Entrypoint;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class RequestDto {
    public function __construct(private UploadedFile $file) {}
}