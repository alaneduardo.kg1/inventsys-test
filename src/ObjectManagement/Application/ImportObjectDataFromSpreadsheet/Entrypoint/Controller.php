<?php

declare(strict_types=1);

namespace App\ObjectManagement\Application\ImportObjectDataFromSpreadsheet\Entrypoint;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class Controller extends AbstractController
{
    #[Route('/objects/spreadsheet-data', methods: ["POST"])]
    public function number(Request $request): Response
    {
        $file = $request->files->get('file');

        $statusCode = Response::HTTP_OK;

        if ($this->isNotFileTypeSupported($file)) {
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
        }

        return new Response("", $statusCode);
    }

    private function isNotFileTypeSupported(UploadedFile $file): bool 
    {
        return !in_array(
            $file->getClientMimeType(), 
            [
                'text/csv', 
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
                'application/vnd.ms-excel.sheet.macroEnabled.12',
                'application/vnd.ms-excel.template.macroEnabled.12',
                'application/vnd.ms-excel.addin.macroEnabled.12',
                'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            ]
        );
    }
}
