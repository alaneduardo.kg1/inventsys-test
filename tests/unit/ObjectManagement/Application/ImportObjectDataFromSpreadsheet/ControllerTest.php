<?php

namespace UnitTest\ObjectManagement\Application\ImportObjectDataFromSpreadsheet\Entrypoint;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

class ControllerTest extends WebTestCase
{
    private const RESOURCES_BASE_PATH = "./tests/unit/resources/";

    public static function invoke_AllowedFileTypes_OkResponse_DataProvider(): array
    {
        return [
            ['FileToImport.csv', 'text/csv'],
            ['FileToImport.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        ];
    }

    #[Test]
    #[DataProvider('invoke_AllowedFileTypes_OkResponse_DataProvider')]
    public function invoke_AllowedFileTypes_OkResponse(string $file, string $mimeType): void
    {
        $client = static::createClient();

        $uploadedFile = new UploadedFile(
            self::RESOURCES_BASE_PATH . $file, 
            $file,
            $mimeType
        );

        $crawler = $client->request(
            Request::METHOD_POST, 
            '/objects/spreadsheet-data',
            [],
            ['file' => $uploadedFile ]
        );

        $this->assertResponseIsSuccessful();
    }

    public static function invoke_NotAllowedFileTypes_UnprocessableEntityResponse_DataProvider(): array
    {
        return [
            ['FileToImport.doc', 'application/msword'],
            ['FileToImport.pdf', 'application/pdf'],
        ];
    }

    #[Test]
    #[DataProvider('invoke_NotAllowedFileTypes_UnprocessableEntityResponse_DataProvider')]
    public function invoke_NotAllowedFileTypes_UnprocessableEntityResponse(string $file, string $mimeType): void
    {
        $client = static::createClient();

        $uploadedFile = new UploadedFile(
            self::RESOURCES_BASE_PATH . $file, 
            $file,
            $mimeType
        );

        $crawler = $client->request(
            Request::METHOD_POST, 
            '/objects/spreadsheet-data',
            [],
            ['file' => $uploadedFile ]
        );

        $this->assertResponseIsUnprocessable();
    }
}