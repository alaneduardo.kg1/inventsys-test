Feature: Import spreadsheet with objects via API

  Scenario Outline: Allowed file types
    Given I have a <file_type> file with objects to be imported
    When I try to import the file
    Then the request is sent successfully
  Examples:
  | file_type |
  | CSV       | 
  | XLSX      |

  Scenario Outline: Not allowed file types
    Given I have a <file_type> file with objects to be imported
    When I try to import the file
    Then the request is not sent successfully
  Examples:
  | file_type |
  | PDF       | 
  | DOC       |