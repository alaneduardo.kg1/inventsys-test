<?php

declare(strict_types=1);

namespace AcceptanceTest\Context\ImportObjectDataFromSpreadsheet;

use Behat\Behat\Context\Context;
use Symfony\Component\HttpClient\Response\NativeResponse;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

use function PHPUnit\Framework\assertEquals;

class Steps implements Context
{
    private const RESOURCES_BASE_PATH = "./tests/acceptance/resources/";

    private const BASE_URI = "http://localhost";

    private const ENDPOINT_TO_BE_TESTED = "/objects/spreadsheet-data";

    private array $allowedFilesMapping = [
        "CSV" => "FileToImport.csv",
        "XLSX" => "FileToImport.xlsx",
        "DOC" => "FileToImport.doc",
        "PDF" => "FileToImport.pdf",
    ];

    private string $fileToBeImported;

    private NativeResponse $response;

    /**
     * @Given I have a :fileType file with objects to be imported
     *
     * @param string $fileType
     */
    public function iHaveAFileTypeFileWithObjectsToBeImported(
        string $fileType
    ): void {
        $this->fileToBeImported = $this->allowedFilesMapping[$fileType];
    }

    /**
     * @When I try to import the file
     */
    public function iTryToImportTheFile()
    {
        $client = HttpClient::createForBaseUri(self::BASE_URI);

        $formData = new FormDataPart([
            'file' => DataPart::fromPath(self::RESOURCES_BASE_PATH . $this->fileToBeImported),
        ]);

        $this->response = $client->request(
            Request::METHOD_POST, 
            self::ENDPOINT_TO_BE_TESTED, 
            [
                "headers" => $formData->getPreparedHeaders()->toArray(),
                "body" => $formData->bodyToIterable()
            ]
        );
    }

    /**
     * @Then the request is sent successfully
     */
    public function theRequestIsSentSuccessfully()
    {
        assertEquals(Response::HTTP_OK, $this->response->getStatusCode());
    }

    /**
     * @Then the request is not sent successfully
     */
    public function theRequestIsNotSentSuccessfully()
    {
        assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $this->response->getStatusCode());
    }
}
